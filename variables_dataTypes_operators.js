const str1 = 'Filip'
let str2 = ''

const numb1 = '1'
let numb2 = '10.2'

const bool1 = true
let bool2 = false

let nullExample = null
const undefinedExample = undefined

console.log(`1 - ${str1}`)
console.log(`2 - ${str2}`)
console.log(`3 - ${numb1}`)
console.log(`4 - ${numb2}`)
console.log(`5 - ${bool1}`)
console.log(`6 - ${bool2}`)
console.log(`7 - ${nullExample}`)
console.log(`8 - ${undefinedExample}`)

console.log()
console.log(typeof(str1))
console.log(typeof(str2))
console.log(typeof(numb1))
console.log(typeof(numb2))
console.log(typeof(bool1))
console.log(typeof(bool2))
console.log(typeof(nullExample))
console.log(typeof(undefinedExample))

console.log()

resultOfSum = numb1 + numb2
resultOfSubtraction = numb1 - numb2
resultOfMultiplication = numb1 * numb2
resultOfDivision= numb1 / numb2

console.log(resultOfSum)
console.log(resultOfSubtraction)
console.log(resultOfMultiplication)
console.log(resultOfDivision)

str2 = 'Galatasaray is my favourite club'
console.log(`\n${str1} ${str2}`)

newStr = String(numb1)
newStr2 = String(numb2)

console.log()
console.log(newStr)
console.log(newStr2)

console.log()
console.log(nullExample == undefinedExample)
console.log(nullExample === undefinedExample)