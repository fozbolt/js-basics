// 1. zadatak
function factorial(numb){

      if (numb === 0 || numb === 1) return 1;

      for (var i = numb - 1; i >= 1; i--) {
         numb = numb * i; 
        }

return numb; 
}

console.log(factorial(5))


// 2. zadatak
let userInfo = {
    name: 'Filip',
    birthdate: Date('02/02/2020'), //cisto za primjer
    gender: 'male',
    hasAPet: true,
    numberOfCountriesVisited: 5
}

const cloneOne = {...userInfo}
//const cloneOne = userInfo

let cloneNamedDifferent = {}; 

for (let key in userInfo) {

    if(key === 'name'){
        cloneNamedDifferent[key] = 'Zvonko'
    } else{
        cloneNamedDifferent[key] = userInfo[key];
    }
    
}

console.log(cloneNamedDifferent)
console.log(userInfo === cloneOne) //explanation: They have different adress reference so they are different, if we just assign value with = then "true" is returned

adressInfo = {
    adress: 'Goranska 50'
}

userInfo.adress  =adressInfo.adress
cloneNamedDifferent.adress  =adressInfo.adress

console.log(cloneNamedDifferent.adress === userInfo.adress)


//3. zadatak
function deleteField (arrayOfObjects, fieldName){
    arrayOfObjects.forEach(obj => {
        delete obj[fieldName]
    })
    return arrayOfObjects
}

arrayWithObjects = [
    userInfo1 = {
        name: 'Filip',
        birthdate: Date('02/02/2020'), //cisto za primjer
        gender: 'male',
        hasAPet: true,
        numberOfCountriesVisited: 5
    },
    userInfo2 = {
        name: 'Marko',
        birthdate: Date('02/02/2020'), 
        gender: 'male',
        hasAPet: true,
        numberOfCountriesVisited: 5
    },
    userInfo3 = {
        name: 'Darko',
        birthdate: Date('02/02/2020'), 
        gender: 'male',
        hasAPet: true,
        numberOfCountriesVisited: 5
    }
]


const result = deleteField(arrayWithObjects, 'birthdate')

console.log(result)


// 4. zadatak

function Rectangle(a, b) {
    this.length = a
    this.width = b
    this.perimeter = 2*(a+b)
    this.area = a*b
  }

  let rect = new Rectangle(5,4)
  console.log(rect)


  // 5. zadatak

getMaxAndMin = numbers => {
    let max = -1
    let min = 101

    numbers.forEach(function(number) {
        if (number> max) max = number
        if (number< min) min = number
    });

    return {max, min}
}

n=10
numbersArray = Array.from({length: n}, () => Math.floor(Math.random() * 100))
//console.log(numbersArray)

console.log(getMaxAndMin(numbersArray))
