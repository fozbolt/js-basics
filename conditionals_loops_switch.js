function getGrade(score) {
    let grade;

    if (score<=30 && score>=0){
        grade = 'F'
    } else if(score<=45 && score>=31){
        grade = 'D'
    } else if(score<=65 && score>=46){
        grade = 'C'
    } else if(score<=84 && score>=66){
        grade = 'B'
    } else if(score<=100 && score>=85){
        grade = 'A'
    }
  
    return grade;
  }
  
  function getAverageAge(ageArray) {

    let sum = 0;
    for (i=0;i<ageArray.length;i++){
        sum += ageArray[i]
    }
    return sum/ageArray.length;
  }
  
  function getDayName(dayNumber) {
    // dayNumber should only be between 1-7 (including both). 
    switch(dayNumber){
        case 1: console.log('Monday') 
            break;
        case 2: console.log('Tuesday') 
            break;
        case 3: console.log('Wednesday') 
            break;
        case 4: console.log('Thursday') 
            break;
        case 5: console.log('Friday') 
            break;
        case 6: console.log("It's weekend") 
            break;
        case 7: console.log("It's weekend") 
            break;

        default: console.log('invalid input')
    }
    
  }
  
  console.log(getGrade(86));
  console.log(getGrade(40));
  
  console.log(getAverageAge([12, 55, 64, 34, 90, 53, 29]));
  console.log(getAverageAge([4, 6, 29, 68, 44]));
  
  console.log(getDayName(3));
  console.log(getDayName(6));
  console.log(getDayName(19));